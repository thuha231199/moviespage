import logo from './logo.svg';
import './App.css';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import HomePage from './Page/HomePage/HomePage';
import LoginPage from './Page/LoginPage/LoginPage';
import DetailPage from './Page/DetailPage/DetailPage';
import Layout from './Components/Layout/Layout';
import BookingPage from './Page/BookingPage/BookingPage';
import NotFoundPage from './Page/NotFoundPage';
import { adminRoute } from './routes/adminRoutes';
import Spinners from './Components/Spinners/Spinners';

function App() {
  return (
    <div> 
       <Spinners/>

      <BrowserRouter>
     
      <Routes>
      <Route>
        {adminRoute.map(({url,component}) => {
          return <Route 
          path={url}
          element={component}/>
        })}
      </Route>
      </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
