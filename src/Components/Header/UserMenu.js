import React from 'react'
import { useSelector } from 'react-redux'
import { NavLink } from 'react-router-dom';
import { localUserServ } from '../../service/localService';


export default function UserMenu() {
  let userInfo = useSelector((state) => {
    return state.userReducer.userInfo;
  });
  //null => false
  // {} => true

let handleLogout=()=> {
  localUserServ.remove();
  //khi trang web reload cho trang login reload luôn
  // window.location.href="/login"
  

  //tải lại trang tại vị trí hiện tại
  window.location.reload();
}

  let renderContent = () => {
    if (userInfo) {
      return (
      <>
      <span>{userInfo.hoTen}</span>
      <button 
      onClick={handleLogout}
      className='px-5 py-2 rounded border-2 border-black'>Đăng xuất</button>
      </>
      );
    } else {
      return (
        <>
  <NavLink to={"/login"}> 
  <button className='px-5 py-2 rounded border-2 border-black'>Đăng nhập</button>
  </NavLink>
 <button className='px-5 py-2 rounded border-2 border-black'>Đăng xuất</button>
        </>
      );
    }
  };
  return (
    <div className='space-x-5'>{renderContent()}</div>
  )
}
