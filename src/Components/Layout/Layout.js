import React from 'react'
import Header from '../Header/Header'
import Footer from '../Footer/Footer'


export default function Layout({Component}) {
  return (
    <div className='space-y-10 h-full min-h-screen flex flex-col'>
        <Header/>
       <div className='flex-grow'>
       <Component/>
       </div>
        <Footer/>
    </div>
  )
}
