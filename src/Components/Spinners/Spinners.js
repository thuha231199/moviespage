import useSelection from 'antd/es/table/hooks/useSelection'
import React from 'react'
import { PacmanLoader } from 'react-spinners'

export default function Spinners() {
    let {isLoading} = useSelection((state) => state.spinnerSlice)
  return isLoading? (
    <div
    style={{background:"#fca311"}}
    className='fixed w-screen h-screen top-0 left-0 flex justify-center items-center z-50'>
        <PacmanLoader
        size ={200}
        speedMultiplier={2}
        color='#14213d'/>
    </div>
  ) : 
  (
  <></>
  )
}
// isLoading? -> isLoading true thì hiển thị thẻ div
