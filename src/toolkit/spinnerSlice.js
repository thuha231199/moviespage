import { createSlice } from "@reduxjs/toolkit";

let initialState={
    isLoading: false,
};

const spinnerSlice = createSlice({
    name:"spinnerSlice",
    initialState,
    reducers:{
        setLoadingOn:(state, action)=>{
            state.isLoading = true;
        },
        setLoadingOff:(state,action) => {
            state.isLoading = false;
        }
    }
})

export const {setLoadingOn , setLoadingOff}= spinnerSlice.actions;
export default spinnerSlice.reducer;
// update giá trị trên store nhưng giao diện k render lại


// Kiểu dữ liệu js 2 nhóm : 1- pass by value (string, number, boolean, null, undefined....)/ 2- pass by reference (array, object)
