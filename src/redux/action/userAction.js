import { SET_USER_LOGIN } from "../constant/userConstant";
import { userServ } from "../../service/userService";
import { localUserServ } from "../../service/localService";
import { message } from "antd";


export const setLoginAction=(values) => {
    return {
        // values đến từ res của api
        type: SET_USER_LOGIN,
        payload: values,
    }
};

export const setLoginActionService = (values, onSuccess) => 
{ 
    //values đến từ form của antd
    return (dispatch) =>  {
        // sau khi gọi API thành công, sẽ đá thêm 1 lần nữa -> nhờ dispatch
        userServ
        .login(values)
        .then ((res)=>{
            dispatch ({
                type: SET_USER_LOGIN,
                payload: res.data.content,
                //gọi API là bất đồng bộ
            });
            localUserServ.set((res.data.content));
            onSuccess();
            //  message.success("Đăng nhập thành công");
            // navigate("/");
        })
        .catch((err)=>{
            console.log(err)
        })
    }
}

//abc123
//12345