//rxreducer
// trong initialSate có key chứa thông tin user/ nếu để object, array rỗng -> true
// sẽ tự động chạy khi user load trang
import { localUserServ } from "../../service/localService";
import { SET_USER_LOGIN } from "../constant/userConstant";



const initialState = {
    userInfor: localUserServ.get(),
    //nếu có dữ liệu trả về object, không có thì trả về null
}


let userReducer = (state = initialState, { type, payload }) => {
  switch (type) {

  case SET_USER_LOGIN:
    return { ...state, userInfo: payload }

  default:
    return state;
  }
};
export default userReducer;