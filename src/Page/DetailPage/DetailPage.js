import React, { useEffect, useState } from 'react'
import { NavLink, useParams } from 'react-router-dom'
import { movieServ } from '../../service/movieService';
import { Progress } from 'antd';

export default function  DetailPage() {
  let params= useParams();
  const [movie, setMovie] = useState({});
  // object rỗng sẽ không bị lỗi
  useEffect(()=>{
    //call api get detail by id:

    // movieServ.getDetailMovie(params.id)
    // .then
    // .catch

    let fetchDetail = async() => {
      try{
        let result = await movieServ.getDetailMovie(params.id)
        setMovie(result.data.content)
       
      } catch (error) {}
       }
       fetchDetail();
  },[])
  return (
    <div className='container flex'>
      <img className='w-1/4' src={movie.hinhAnh} alt="" />
      <div className='p-5 space-y-10'>
        <h2 className='text-xl font-medium'>{movie.tenPhim}</h2>
        <p className='text-xs text-gray-600'>{movie.moTa}</p>
        <Progress 
        format={(percent) => `${percent/10} Điểm`}
        type="circle" percent={movie.danhGia * 10} />
        {/* * 10 : ra độ dài mày xanh; /10 để ra 7 điểm */}
        <NavLink 
        to = {`/booking/${movie.maPhim}`}
        className="px-5 py-2 rounded bg-red-500 text-white">Mua vé</NavLink>
      </div>
    </div>
  )
}
