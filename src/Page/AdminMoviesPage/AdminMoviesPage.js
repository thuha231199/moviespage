import React, {useState, useEffect} from 'react'
import { adminServ } from '../../service/adminService'
import { Table } from 'antd'
import { headerColums } from './utils'
import Spinners from '../../Components/Spinners/Spinners'
import { setLoadingOff, setLoadingOn } from '../../toolkit/spinnerSlice'
import { useDispatch } from 'react-redux'


export default function AdminMoviesPage() {
    const [movieList, setMovieList] = useState([]);
    let dispatch = useDispatch();
    useEffect(() => {
      dispatch(setLoadingOn());
        adminServ
        .getMovieList()
        .then((res)=>{
            dispatch(setLoadingOff());
            //table atnd
            setMovieList(res.data.content)
        })
        .catch((err)=>{
            dispatch(setLoadingOff());
            console.log(err);
        })
    
    }, [])
    console.log(movieList.length);
    
  return (
    <div>
        <Spinners/>
        <Table 
        columns={headerColums}
        dataSource={movieList}/>
    </div>
  )
}

//react spinner
//git fetch
//git checkout admin
