import { Tag } from "antd";

export const headerColums = [
    {
        title:"Id",
        dataIndex:"maPhim",
        key:"tenPhim",
    },
    {
        title: "Tên phim",
        dataIndex: "tenPhim",
        key: "tenPhim"
    },
    {
        title:"Đánh giá",
        dataIndex:"danhGia",
        key:"danhGia",
    },
    {
        title:"Hình ảnh",
        dataIndex:"hinhAnh",
        key:"danhGia",
        render:(url) => {
            return <img src={url} className="w-20" alt=""/>
        }
    },
];
/**
 * {
 * "taiKhoan": "111111",
 * "hoTen": "1234",
 * "email": "cuphong2003hk@gmail.com",
 * "soDT": "2133412343214",
 * "matKhau":"12345",
 * "maLoaiNguoiDung": "KhachHang"}
 */