import moment from 'moment/moment';
import React from 'react'

export default function ItemTabsMovie({ phim }) {
  console.log("🚀 ~ phim:", phim);
  return (
    <div className='flex items-center space-x-5 border-black border-b pb-3'>
        <img 
        className='w-28 h-48 object-cover object-top rounded'
        src={phim.hinhAnh} 
        alt="" />
        <div>
            <h5 className='font-medium text-xl mb-5'>{phim.tenPhim}</h5>
        </div>

        <div className='grid gap-5 grid-cols-3'>
        {phim.lstLichChieuTheoPhim.slice(0, 6).map((item) => {
            return (
                <span className='rounded p-3 text-white bg-red-500 font-medium'>{moment(item.ngayChieuGioChieu).format("DD-mm-yyyy ~ hh-mm")}
                </span>
                //format: object moment -> truyền vào 1 object -> cho custom
                //d: là ngày trong tuần
            )
           
        })}
        </div>
    </div>
  )
}
