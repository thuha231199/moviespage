import { Tag } from "antd";

export const headerColums = [
    {
        title:"Tài Khoản",
        dataIndex:"taiKhoan",
        key:"taiKhoan",
    },
    {
        title: "Tên người dùng",
        dataIndex: "hoTen",
        key: "hoTen"
    },
    {
        title:"Gmail",
        dataIndex:"email",
        key:"email",
    },
    {
        title:"Người dùng",
        dataIndex:"maLoaiNguoiDung",
        key:"maLoaiNguoiDung",
        render:(loai) => {
            if (loai == "KhachHang") {
                return (<Tag className="font-medium" color="green">Khách Hàng</Tag>)
            }
            else {
                return <Tag
                className="font-medium animate-pulse" 
                color="red">Quản Trị</Tag>
            }
        }
    },
];
/**
 * {
 * "taiKhoan": "111111",
 * "hoTen": "1234",
 * "email": "cuphong2003hk@gmail.com",
 * "soDT": "2133412343214",
 * "matKhau":"12345",
 * "maLoaiNguoiDung": "KhachHang"}
 */