import React, {useState, useEffect} from 'react'
import { adminServ } from '../../service/adminService'
import { Table } from 'antd'
import { headerColums } from './utils'
import Spinners from '../../Components/Spinners/Spinners'
import { setLoadingOff, setLoadingOn } from '../../toolkit/spinnerSlice'
import { useDispatch } from 'react-redux'


export default function AdminUsersPage() {
    const [userList, setUserList] = useState([]);
    let dispatch = useDispatch();
    useEffect(() => {
      dispatch(setLoadingOn());
        adminServ
        .getUserList()
        .then((res)=>{
            dispatch(setLoadingOff());
            //table atnd
            setUserList(res.data.content)
        })
        .catch((err)=>{
            dispatch(setLoadingOff());
            console.log(err);
        })
    
    }, [])
    console.log(userList.length);
    
  return (
    <div>
        <Spinners/>
        <Table 
        columns={headerColums}
        dataSource={userList}/>
    </div>
  )
}

//react spinner
//git fetch
//git checkout admin
