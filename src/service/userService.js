// file quản lý API : đăng nhập, đăng kí
import axios from "axios";
import {BASE_URL , configHeaders} from "./config"
import { https } from "./config";

export const userServ = {
    login : (loginData) => {
        return axios ({
            url: `${BASE_URL}/api/QuanLyNguoiDung/DangNhap`,
            method: "POST",
            data: loginData,
            headers: configHeaders(),
        });
    },
    getUserList:() => {
        return https.get("/api/QuanLyNguoiDung/LayDanhSachNguoiDung?MaNhom=GP01")
    }
}

// userServ.login();

// những API quản lí movie