// lấy danh sách phim, lấy chi tiết phim: giờ chiếu ngày chiếu
import axios from "axios";
import { https } from "./config"
// import BASE_URL, configHeadersB
export const movieServ = {
    
    getMovieByTheater:() => {
        return https.get("/api/QuanLyRap/LayThongTinLichChieuHeThongRap")
        // .get -> method : GET
    },
    getDetailMovie:(id)=>{
        return https.get(`/api/QuanLyPhim/LayThongTinPhim?MaPhim=${id}`)
    }
};

// axios instance