import AdminLayout from "../Components/Layout/AdminLayout";
import AdminMoviesPage from "../Page/AdminMoviesPage/AdminMoviesPage";
import AdminUsersPage from "../Page/AdminUsersPage/AdminUsersPage";

export const adminRoute = [
    {
        url:"/admin-users",
        component: <AdminLayout Component={AdminUsersPage}/>
        //<AdminUsersPage/> -> là 1 html hoàn chỉnh
    },
    {
        url:"/",
        component:<AdminLayout Component={AdminUsersPage}/>
    },
    {
        url:"/admin-movies",
        component: <AdminLayout Component={AdminMoviesPage}/>
    }
]